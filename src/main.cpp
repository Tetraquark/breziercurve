#include <SFML/Graphics.hpp>

#include <iostream>
#include <vector>

#include "ControlVertex.h"
#include "Line.h"
#include "BezierCurve.h"

/**
*	Small program which releases algorithm of Bezier fourth order curves,
*	and builds it using SFML library.
*
*   @author     Tetraquark
*   @change     22.07.2015  Tetraquark
*   @docdate    05.04.2015
*
*/

#define WIN_WIDTH           512
#define WIN_HEIGHT          512
#define MAX_CTRL_POINTS     4

int main(){
    sf::RenderWindow window(sf::VideoMode(WIN_WIDTH, WIN_HEIGHT), "Bezier Curves");

    BezierCurve bCurve(5, 100, sf::Color::Cyan);    // Example of some BrezierCurve object
    sf::Vector2i mousePos;

    int activePointId = -1;             /// Selected control point ID
    bool activeLeftButton = false;      /// If left mouse button is pressed
    bool hasChanged = false;            /// Boolean flag for check control points state

    while (window.isOpen()){
        sf::Event event;
        sf::Vector2i mousePos = sf::Mouse::getPosition(window);

        while (window.pollEvent(event)){    /// Event cycle
            switch(event.type){
                case sf::Event::Closed: {
                    window.close();
                    break;
                }
                case sf::Event::MouseButtonPressed: {
                    if(event.key.code == sf::Mouse::Middle){    /// Middle button - add new control point
                        if(bCurve.getCntrlVertexNum() < MAX_CTRL_POINTS){
                            bCurve.addControlVertex(mousePos.x, mousePos.y);
                            hasChanged = true;
                        }
                    }

                    if(event.key.code == sf::Mouse::Left){      /// Left button - move control point
                        if(!bCurve.isCntrlVertexEmpty()){
                            for(int i = 0; i < bCurve.getCntrlVertexNum(); i++){
                                if(bCurve.cmpCntrlVertPos(i, mousePos.x, mousePos.y)){
                                    activeLeftButton = true;
                                    hasChanged = true;
                                    break;
                                }
                            }
                        }
                    }

                    if(event.key.code == sf::Mouse::Right){     /// Right button - remove control point
                        if(!bCurve.isCntrlVertexEmpty()){
                            for(int i = 0; i < bCurve.getCntrlVertexNum(); i++){
                                if(bCurve.cmpCntrlVertPos(i, mousePos.x, mousePos.y)){
                                    bCurve.removeCntrlPoint(i);
                                    hasChanged = true;
                                    break;
                                }
                            }

                        }
                    }
                    break;
                }

                case sf::Event::MouseButtonReleased:{
                    if(event.key.code == sf::Mouse::Left){
                        if(!bCurve.isCntrlVertexEmpty()){
                            activeLeftButton = false;
                            activePointId = -1;
                            hasChanged = true;
                        }
                    }
                    break;
                }
            }
        }

        if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && activeLeftButton){
            if(activePointId != -1){        /// Changing control point position
                bCurve.setPosContrPoint(activePointId, mousePos.x - bCurve.getCntrlVertRadius(), mousePos.y - bCurve.getCntrlVertRadius());
                hasChanged = true;
            }
            else{           /// Else find active control point
                for(int i = 0; i < bCurve.getCntrlVertexNum(); i++){
                    if(bCurve.cmpCntrlVertPos(i, mousePos.x, mousePos.y)){
                        activePointId = i;
                    }
                }
            }
        }

        if(hasChanged)      /// Calculate if state of the object has changed
            bCurve.calcLines();

        window.clear();

        bCurve.draw(&window);

        window.display();
        hasChanged = false;
    }
    return 0;
}
