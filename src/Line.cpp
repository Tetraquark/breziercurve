#include <Line.h>

/*!
* Line Constructor:
* @param    _x  - x0 coordinate
* @param    _y  - y0 coordinate
* @param    _x1 - x1 coordinate
* @param    _y1 - y1 coordinate
*/
Line::Line(float _x, float _y, float _x1, float _y1){
    line = new sf::Vertex[2];
    line[0] = sf::Vertex(sf::Vector2f(_x, _y));
    line[1] = sf::Vertex(sf::Vector2f(_x1, _y1));
}

/*!
* Line Destructor:
* Delete line
*/
Line::~Line(){
    delete[] line;
}
