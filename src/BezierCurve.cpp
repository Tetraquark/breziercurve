#include <BezierCurve.h>

/*!
* Default BezierCurve Constructor:
*/
BezierCurve::BezierCurve(){
    cntrPointRadius = 5;
    pointsNum = 100;
    cntrPointColor = sf::Color::Cyan;
}

/*!
* BezierCurve Constructor:
* @param    radius  - Radius of Control point
* @param    pNum    - Curve segments number
*/
BezierCurve::BezierCurve(float radius, int pNum, sf::Color ctrlColor){
    cntrPointColor = ctrlColor;
    cntrPointRadius = radius;
    pointsNum = pNum;
}

/*!
* BezierCurve Destructor:
* Clears all vectors of class
*/
BezierCurve::~BezierCurve(){
    bezPoints.clear();
    lines.clear();
}

/*!
* Private method calcPoint():
* Calculates the position of the curve point.
* @param    t   - Describes the position of the segment on the curve
*/
sf::Vector2f* BezierCurve::calcPoint(float t){
    float u = 1 - t;
    sf::Vector2f *res = new sf::Vector2f();

    switch(bezPoints.size()){
        case 2: {
            res->x = u * bezPoints[0]->x + t * bezPoints[1]->x;
            res->y = u * bezPoints[0]->y + t * bezPoints[1]->y;
            res->x += cntrPointRadius;
            res->y += cntrPointRadius;
            return res;
            break;
        }
        case 3: {
            res->x = u * u * bezPoints[0]->x + u * t * bezPoints[1]->x + t * t * bezPoints[2]->x;
            res->y = u * u * bezPoints[0]->y + u * t * bezPoints[1]->y + t * t * bezPoints[2]->y;
            res->x += cntrPointRadius;
            res->y += cntrPointRadius;
            return res;
            break;
        }
        case 4: {
            res->x = u*u*u * bezPoints[0]->x + 3 * t * u*u * bezPoints[1]->x + 3 * t*t * u * bezPoints[2]->x + t*t*t * bezPoints[3]->x;
            res->y = u*u*u * bezPoints[0]->y + 3 * t * u*u * bezPoints[1]->y + 3 * t*t * u * bezPoints[2]->y + t*t*t * bezPoints[3]->y;
            res->x += cntrPointRadius;
            res->y += cntrPointRadius;
            return res;
        break;
        }
        default: {
            delete res;
            return nullptr;
            break;
        }
    }
    delete res;
    return nullptr;
}

/*!
* Public method calcLines():
* Calculates the position of the curve segment.
*/
void BezierCurve::calcLines(){
    clearLines();
    lines.shrink_to_fit();

    sf::Vector2f* v0 = calcPoint(0);
    sf::Vector2f* v1;
    float t = 0;
    for(int i = 0; i < pointsNum; i++){
        t += (float) 1 / pointsNum;
        v1 = calcPoint(t);
        if(v1 != nullptr){
            lines.push_back(new Line(v0->x, v0->y, v1->x, v1->y));
            v0 = v1;
        }
    }
    delete v0;
    delete v1;
}

/*!
* Public method draw():
* Draws segments and control points of curve
* @param    *window     - Pointer to the application's main window
*/
void BezierCurve::draw(sf::RenderWindow *window){

    if(!bezPoints.empty()){
        for(int i = 0; i < bezPoints.size(); i++)
            bezPoints[i]->draw(window);

        if(bezPoints.size() >= 2){
            sf::Vertex line12[] = {
                sf::Vertex(sf::Vector2f(bezPoints[0]->x + cntrPointRadius, bezPoints[0]->y + cntrPointRadius)),
                sf::Vertex(sf::Vector2f(bezPoints[1]->x + cntrPointRadius, bezPoints[1]->y + cntrPointRadius))
            };
            line12[0].color = sf::Color::Red;
            line12[1].color = sf::Color::Red;
            window->draw(line12, 2, sf::Lines);
            if(bezPoints.size() >= 4){
                sf::Vertex line34[] = {
                    sf::Vertex(sf::Vector2f(bezPoints[2]->x + cntrPointRadius, bezPoints[2]->y + cntrPointRadius)),
                    sf::Vertex(sf::Vector2f(bezPoints[3]->x + cntrPointRadius, bezPoints[3]->y + cntrPointRadius))
                };
                line34[0].color = sf::Color::Red;
                line34[1].color = sf::Color::Red;
                window->draw(line34, 2, sf::Lines);
            }
        }
    }

    if(bezPoints.size() > 1)
        for(int i = 0; i < lines.size(); i++){
            sf::Vertex *line = lines[i]->line;
            window->draw(line, 2, sf::Lines);
        }
}

/*!
* Public method setPosContrPoint():
* Setter of control point positions
* @param    id  - identifier of control point in vector
* @param    _x  - x coordinate
* @param    _y  - y coordinate
*/
void BezierCurve::setPosContrPoint(int id, float _x, float _y){
    bezPoints[id]->x = _x;
    bezPoints[id]->y = _y;
}

/*!
* Public method addControlVertex():
* Push back new control point
* @param    _x  - x coordinate
* @param    _y  - y coordinate
*/
void BezierCurve::addControlVertex(float _x, float _y){
    bezPoints.push_back(new ControlVertex(_x, _y, cntrPointRadius, cntrPointColor));
}

/*!
* Public method removeCntrlPoint():
* Delete the control point by id
* @param    id  - identifier of control point in vector
*/
void BezierCurve::removeCntrlPoint(int id){
    delete bezPoints[id];
    bezPoints.erase(bezPoints.begin() + id);
}

/*!
* Public method getCntrlVertexNum():
* Getter of control points number
*/
int BezierCurve::getCntrlVertexNum(){
    return bezPoints.size();
}

/*!
* Public method getCntrlVertRadius():
* Getter of control points radius
*/
int BezierCurve::getCntrlVertRadius(){
    return cntrPointRadius;
}

/*!
* Public method getPointsNum():
* Getter of curve segments
*/
int BezierCurve::getPointsNum(){
    return pointsNum;
}

/*!
* Public method isCntrlVertexEmpty():
* Getter of vector size status
*/
bool BezierCurve::isCntrlVertexEmpty(){
    return bezPoints.empty();
}

/*!
* Public method cmpCntrlVertPos():
* Compare control point position with input coordinates
* @param    id  - identifier of control point in vector
* @param    _x  - comparable  x coordinate
* @param    _y  - comparable  y coordinate
*/
bool BezierCurve::cmpCntrlVertPos(int id, float _x, float _y){
    return bezPoints[id]->cmpPos(_x, _y);
}

/*!
* Private method clearLines():
* Clear segments vector
*/
void BezierCurve::clearLines(){
    for(int i = 0; i < lines.size(); i++){
        delete lines[i];
        lines.erase(lines.begin() + i);
        i--;
    }
}
