#ifndef BEZIERCURVE_H_INCLUDED
#define BEZIERCURVE_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <ControlVertex.h>
#include <Line.h>

class BezierCurve{

private:
    std::vector <ControlVertex*> bezPoints; /// Vector of control points
    std::vector <Line*> lines;              /// Vector of Bezier Curve segments

    float cntrPointRadius;                  /// Control points radius
    sf::Color cntrPointColor;               /// Color of control points
    int pointsNum;                          /// Segments number of Bezier Curve

    sf::Vector2f* calcPoint(float t);
    void clearLines();
public:

    BezierCurve();
    BezierCurve(float radius, int pNum, sf::Color ctrlColor);
    ~BezierCurve();

    void calcLines();
    void draw(sf::RenderWindow *window);

    void setControlPointsRadius(float R);
    void setPosContrPoint(int id, float _x, float _y);
    void addControlVertex(float mx, float my);
    void removeCntrlPoint(int id);

    int getCntrlVertexNum();
    int getCntrlVertRadius();
    int getPointsNum();
    bool isCntrlVertexEmpty();
    bool cmpCntrlVertPos(int id, float _x, float _y);
};


#endif // BEZIERCURVE_H_INCLUDED
