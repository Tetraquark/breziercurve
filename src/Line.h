#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

#include <SFML/Graphics.hpp>

/*!
* Class of curve segment.
* Includes sf::Vertex objects array which form a line.
*/
class Line{

public:
    sf::Vertex *line;       ///Line of curve segment
    Line(float _x, float _y, float _x1, float _y1);
    ~Line();
};

#endif // LINE_H_INCLUDED
