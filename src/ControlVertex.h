#ifndef CONTROLVERTEX_H_INCLUDED
#define CONTROLVERTEX_H_INCLUDED

#include <SFML/Graphics.hpp>

class ControlVertex{

private:
    sf::Color vertexColor;          /// Color of control point
    sf::CircleShape *vertexShape;   /// Shape of control point
    float radius;                   /// Control point radius
public:
    float x, y;

    ControlVertex(float x0, float y0, float r, sf::Color color);
    ~ControlVertex();

    void draw(sf::RenderWindow *window);
    bool cmpPos(int _x, int _y);
};

#endif // CONTROLVERTEX_H_INCLUDED
