#include <ControlVertex.h>

/*!
* ControlVertex Constructor:
* @param    _x      - x coordinate
* @param    _y      - y coordinate
* @param    r       - radius of point
* @param    color   - color of point
*/
ControlVertex::ControlVertex(float _x, float _y, float r, sf::Color color){
    sf::Vector2f posv(_x, _y);
    radius = r;
    vertexColor = color;

    vertexShape = new sf::CircleShape(radius);
    vertexShape->setFillColor(vertexColor);
    vertexShape->setPosition(_x, _y);

    x = _x;
    y = _y;
}

/*!
* ControlVertex Destructor:
* Delete point shape
*/
ControlVertex::~ControlVertex(){
    delete vertexShape;
}

/*!
* Public method draw():
* Draws point and set object shape position
* @param    *window - Pointer to the application's main window
*/
void ControlVertex::draw(sf::RenderWindow *window){
    vertexShape->setPosition(x, y);
    window->draw(*vertexShape);
}

/*!
* Public method cmpPos():
* Compare point position with input coordinates
* @param    _x  - comparable x coordinate
* @param    _y  - comparable y coordinate
*/
bool ControlVertex::cmpPos(int _x, int _y){
    if(_x >= x && _x <= x + 2 * radius)
        if(_y >= y && _y <= y + 2 * radius)
            return true;
    return false;
}
