# Описание: #
Программа в которой реализуется алгоритм построения кривых Безье первого, второго, третьего и четвертого порядка. Для графического вывода используется открытая библиотека [SFML] (http://www.sfml-dev.org/). Приложение представляет собой окно 512x512px. 

# Управление: #
* Добавление контрольной точки: средняя кнопка мыши в нужном месте окна 
* Удаление контрольной точки: правая кнопка мыши по точке 
* Перемещение контрольной точки: зажатая левая кнопка мыши

Ах да, компилить придется самому =(

# Description: #
Program that realized the algorithm for constructing Bezier curves first, second, third and fourth order. For graphical output using an open library [SFML] (http://www.sfml-dev.org/). The application is a window 512x512px.

# Controls: #
* Adding control point: Middle-click on the window
* Delete control point: Right-click on the control point
* Move control point: hold down left button on control point

![example.jpg](https://bitbucket.org/repo/r5oxa8/images/2190760788-example.jpg)